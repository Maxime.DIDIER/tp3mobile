import { StatusBar } from 'expo-status-bar';
import {StyleSheet, Text, View, Image, FlatList, SafeAreaView, TouchableOpacity} from 'react-native';
import {useEffect, useState} from "react";



export default function App() {
    const [liste, setListe] = useState([])

    const cocktailrandom = () => {
        return fetch('https://www.thecocktaildb.com/api/json/v1/1/random.php')
            .then(function(response){
                return response.json();
            }).then(function(response){
            return response.drinks[0]
        })
    }

    const add = () => {
        let promises = [];
        for (let i = 0; i < 20; i++) {
            promises.push(cocktailrandom());
        }

        Promise.all(promises).then((data) => {setListe([...liste, ...data])})
    }

    useEffect(() => {
        add()
    }, [])

  return (

    <SafeAreaView>
        <Text style={styles.h1}>Application Cocktail</Text>
        {liste && <FlatList
                data={liste}
                style={styles.list}
                onEndReached={add}
                numColumns={2}
                keyExtractor={item => item.idDrink}
                renderItem={({item}) => (
                    <View style={styles.viewss}>
                        <TouchableOpacity>
                            <Text style={styles.text1}>{item.strDrink}</Text>
                            <Image style={styles.img} source={{
                                uri: `${item.strDrinkThumb}`
                            }}/>
                        </TouchableOpacity>
                    </View>
            )}/>}
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    touchable: {

    },
    h1: {
        paddingTop: 20,
        paddingBottom: 20,
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#b71540',
    },
    list : {
        backgroundColor: '#000'
    },
    viewss: {
        flex: 1,
        borderColor: '#e55039',
        borderWidth: 2,
        borderRadius: 3
    },
    text1: {
        fontSize: 15,
        fontFamily: "Arial",
        textAlign: 'center',
        color: '#f8c291'
    },
    img: {
        aspectRatio: 1
    }
});
